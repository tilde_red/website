---
menu: navbar
---

## It's Alive - Updated January 19, 2024

Tilde.red is a member ran cooperative system, based on anti-capitalist, anti-racism, and anti-fascism.

## Roadmap/timeline

The roadmap for what tilde.red likely will look like, and timeline for it coming on-line is:

- Statement of principles completed. By end of month 1 (in progress)
- Community Guildelines completed. By end of month 1 (in progress)
- Unified sign on for all services. Aiming to have this in place after the first two months.
- Shell server should be up and running in the first month. (completed)
- Bring Zulip to be either self-hosted, or sponsored by Zulip. Aiming for this to be settled by month 6.
- Email services should come online in the first month.
- Choosing what set of intial community services to operate, and the scope of usage. (in progress)

The system is alive, and in the middle of a soft launch. Several members are already there, helping to build everything out, but we are certainly looking for more people interested.

## Current State

Currently, chat is up and running, our webserver is up and running, user creation and logins are working, and shell server is up and running.

We are still ironing out the kinks and things are very much in a state of flux at the moment.

## Architecture

The architecture is still in a state of flux, but how its looking right now:

- The chat system we'll be using is Zulip. It is a great open source product, that has apps for all OSs available, a terminal client officially supported, and a very deep API for integrations.
- There will be a segregation of some resources. The webserver will be isolated from the shell box, and the LDAP server will be isolated from all of them. Et cetera.
- The ability to co-administer the system will be able to be done via the RedBot, by most users of the system.
- We're sitting with Ansible for now, to administer the system, as a backend, which RedBot will use.
- A NFS server will be available, so user data can more easily follow the user around with them.
- Remote Tilde VM is in the works. We'd like to be able to have people take a Qemu VM image, download it, log in, and be as much as possible, just like you logged into the common shell box.
- CLI-first, but not only CLI. All services should gracefully, and appealingly, be able to work on the simplest of devices.
- Democratic and equitable management of the system, and services. No gods, no masters, no CEOs, no managers, no vanguards, no central committees.
- A distributed system. Members should be able to add resources to the system, on their own, without too much intervention from anyone else

This is just an abbreviated plan, at this time, and will get fleshed out, and updated as needed.

## Things you will need to know

### Source code

All source code is located at [Codeberg](https://codeberg.org/tilde_red).

Ansible is used to maintain the core infastructure, minimally. It is also used, as desired, by individual users standing services up.


### FAQ
**Q:** Why CLI-first?!?! I suck at using a terminal!  
**A:** It's only an expression. If it can work in a CLI, it should be able to work on any browser, or any device, frankly. Different people have different needs, and ensuring it all "degrades wonderfully" is key to allow freedom to interact as the individual person sees fit.

**Q:** Is this like the old tilde.red?  
**A:** No, but yes. People learned lessons last go around, and we're thinking we can pick up in a better place.

**Q:** How can I get involved?  
**A:** You can shoot an email to possum at tilde.red, for now. Or, hit up @ubergeek@tilde.zone on the Fediverse.

{{< presence >}}
