+++
title = "About tilde.red"
+++

Tilde.red is a member-run cooperative system, based on <span style="white-space: nowrap;">anti-capitalism</span>, <span style="white-space: nowrap;">anti-racism</span>, and <span style="white-space: nowrap;">anti-fascism</span>.

## Principles

We're writing these up at the moment. They'll be published here as soon as we have agreed the wording.

## Guidelines

Another work in progress. Again, watch this space.

## Joining

For now, please just contact Possum at the email below or go on Mastodon and send a DM to @ubergeek@tilde.zone.

{{< presence >}}
